package ru.edu;

import ru.edu.model.Athlete;
import ru.edu.model.CountryParticipant;
import ru.edu.model.CountryParticipantImpl;
import ru.edu.model.Participant;

import java.util.*;
import java.util.stream.Collectors;

public class CompetitionImpl implements Competition {

    private long id = 0;
    private Map<Long, MyParticipant> participantMap = new HashMap<>();
    private Set<Athlete> registeredAthlets = new HashSet<>();

    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(Athlete participant) {
        if(registeredAthlets.contains(participant)){
            throw new IllegalStateException("Duplicate registration");
        }

        registeredAthlets.add(participant);

        MyParticipant internalAndSecuredObject = new MyParticipant(++id, participant);
        participantMap.put(internalAndSecuredObject.id, internalAndSecuredObject);

        return internalAndSecuredObject.getClone();
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(long id, long score) {
        Participant myParticipant = participantMap.get(id);
        if(myParticipant != null){
            myParticipant.incrementScore(score);
        }
        else{
            throw new IllegalArgumentException("Participant do not registration");
        }
    }

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(Participant participant, long score) {
        Participant myParticipant = participantMap.get(participant.getId());
        if(myParticipant != null){
            myParticipant.updateScore(score);
        }
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {

        LinkedList<Participant> myParticipants = new LinkedList<>(participantMap.values());
        myParticipants.sort(Comparator.comparing(Participant::getScore).reversed());
        return myParticipants;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {

        Map<String, CountryParticipantImpl> countryParticipantHashMap = new HashMap<>();

        for (Map.Entry<Long, MyParticipant> value : participantMap.entrySet()) {
            String countryName = value.getValue().getAthlete().getCountry();
            Participant athlete = value.getValue();

            if (countryParticipantHashMap.containsKey(countryName)) {
                CountryParticipantImpl countryParticipant =
                        countryParticipantHashMap.get(countryName);
                long score = athlete.getScore();
                long score_tmp = countryParticipant.getScore();
                countryParticipant.setScore(score + score_tmp);
            }
            else{
                CountryParticipantImpl countryParticipant = new CountryParticipantImpl(countryName);
                long score = athlete.getScore();
                countryParticipant.setScore(score);
                countryParticipantHashMap.put(countryName, countryParticipant);
            }
            CountryParticipantImpl countryParticipant = countryParticipantHashMap.get(countryName);
            countryParticipant.addParticipant(athlete);
        }

        List<CountryParticipant> list = countryParticipantHashMap
                .values()
                .stream()
                .sorted((o1, o2) -> {
                    if (o1.getScore() == o2.getScore()) {
                        return 0;
                    }
                    return o1.getScore() > o2.getScore() ? -1 : 1;
                })
                .collect(Collectors.toList());

        return list;

    }


    /**
     * Получение количества зарегистрированных участников.
     * @return размер списка HashMap
     */
    public int getParticipantMapSize() {
        return participantMap.size();
    }

    /**
     * Получить участиника по ID.
     * @return ссылка на участника
     */
    public MyParticipant getParticipant(long id) {
        return participantMap.get(id);
    }


    private static class MyParticipant implements Participant {
        public final Long id;
        private long score;
        private Athlete athlete;

        public MyParticipant(long id, Athlete athlete) {
            this.id = id;
            this.athlete = athlete;
        }

        /**
         * Получение информации о регистрационном номере.
         *
         * @return регистрационный номер
         */
        @Override
        public Long getId() {
            return id;
        }

        /**
         * Информация о спортсмене
         *
         * @return объект спортсмена
         */
        @Override
        public Athlete getAthlete() {
            return athlete;
        }

        /**
         * Счет участника.
         *
         * @return счет
         */
        @Override
        public long getScore() {
            return score;
        }

        /**
         * Увеличить счет участника.
         *
         */
        public void incrementScore(long value){
            score += value;
        }

        /**
         * Обновить счет участника.
         *
         */
        public void updateScore(long value){
            score = value;
        }


        /**
         * getClone.
         *
         *  @return новый обеъкт с заданными параметрами
         */
        public MyParticipant getClone() {
            return new MyParticipant(this.id, this.athlete);
        }

    }

}
