package ru.edu.model;

import java.util.Objects;

public class ParticipantImpl implements Participant{

    public final Long id;
    private long score;
    private Athlete athlete;

    public ParticipantImpl(long id, Athlete athlete) {
        this.id = id;
        this.athlete = athlete;
    }

    /**
     * Получение информации о регистрационном номере.
     *
     * @return регистрационный номер
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Информация о спортсмене
     *
     * @return объект спортсмена
     */
    @Override
    public Athlete getAthlete() {
        return athlete;
    }

    /**
     * Счет участника.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    /**
     * Увеличить счет участника.
     *
     */
    @Override
    public void incrementScore(long value) {
        score += value;
    }

    /**
     * Обновить счет участника.
     *
     */
    @Override
    public void updateScore(long value) {
        score = value;
    }

    /**
     * getClone.
     *
     *  @return новый обеъкт с заданными параметрами
     */
    @Override
    public Participant getClone() {
        return new ParticipantImpl(this.id, this.athlete);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParticipantImpl)) return false;
        ParticipantImpl that = (ParticipantImpl) o;
        return getId().equals(that.getId())
                && getScore() == that.getScore()
                && Objects.equals(getAthlete(), that.getAthlete());
    }


    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAthlete(), getScore());
    }


}
