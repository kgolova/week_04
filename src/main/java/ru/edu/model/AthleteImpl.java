package ru.edu.model;

public class AthleteImpl implements Athlete {

    /**
     * Имя.
     */
    private String firstName;

    /**
     * Фамилия.
     */
    private String lastName;

    /**
     * Страна.
     */
    private String country;

    public static Builder build() {
        return new Builder();
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }


    /**
     * equals.
     *
     * @return Результат сравнения двух объектов
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AthleteImpl other = (AthleteImpl) o;
        if (firstName != other.firstName)
            return false;
        if (lastName != other.lastName)
            return false;
        if (country != other.country)
            return false;
        return true;
    }

    /**
     * hashCode.
     *
     * @return hashCode объекта
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + firstName.length();
        result = prime * result + lastName.length();
        result = prime * result + country.length();

        return result;
    }

    /**
     * toString.
     *
     * @return строковое представление объекта
     */
    @Override
    public String toString() {
        return "Имя=" + firstName + ' ' + "Фамилия="
                + lastName + ' ' + "(Страна=" + country + ")";
    }

    public static class Builder {

        private AthleteImpl athlete = new AthleteImpl();


        public AthleteImpl build() {
            return athlete;
        }

        /**
         * Установка параметра firstName.
         *
         * @return объект
         */
        public Builder setFirstName(String input) {
            athlete.firstName = input;
            return this;
        }

        /**
         * Установка параметра lastName.
         *
         * @return объект
         */
        public Builder setLastName(String input) {
            athlete.lastName = input;
            return this;
        }

        /**
         * Установка параметра country.
         *
         * @return объект
         */
        public Builder setCountry(String input) {
            athlete.country = input;
            return this;
        }

    }
}
