package ru.edu.model;

import java.util.ArrayList;
import java.util.List;

public class CountryParticipantImpl implements CountryParticipant{

    private String name;
    private Long score;
    private List<Participant> participantList = new ArrayList<>();

    /**
     * @param name
     */
    public CountryParticipantImpl(String name) {
        this.name = name;
    }

    /**
     * Установка счета стране.
     *
     * @param score
     */
    public void setScore(long score) {
        this.score = score;
    }

    /**
     * Название страны.
     *
     * @return название
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Список участников от страны.
     *
     * @return список участников
     */
    @Override
    public List<Participant> getParticipants() {
        return participantList;
    }

    /**
     * Счет страны.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }


    /**
     * Добавление участника соревнования в список участников от страны.
     *
     * @param participant
     */
    public void addParticipant(Participant participant) {
        if (name != participant.getAthlete().getCountry()) {
            throw new IllegalArgumentException("Атлет принадлежит к другой стране.");
        }
        participantList.add(participant);
    }

}
