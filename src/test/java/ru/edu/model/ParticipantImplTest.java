package ru.edu.model;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class ParticipantImplTest extends TestCase {

    private AthleteImpl athlete1;
    private AthleteImpl athlete2;
    private ParticipantImpl participant1;
    private ParticipantImpl participant2;

    @Before
    public void setUp() {

        athlete1 = new AthleteImpl.Builder()
                .setFirstName("Yuma")
                .setLastName("KAGIYAMA")
                .setCountry("Japan")
                .build();

        athlete2 = new AthleteImpl.Builder()
                .setFirstName("Jason")
                .setLastName("BROWN")
                .setCountry("USA")
                .build();

        participant1 = new ParticipantImpl(1, athlete1);
        participant2 = new ParticipantImpl(2, athlete2);

    }

    @Test
    public void testGetId() {

    }

    @Test
    public void testGetAthlete() {
        assertEquals(athlete1, participant1.getAthlete());
        assertEquals(athlete2, participant2.getAthlete());
    }

    @Test
    public void testGetScore() {
        participant1.updateScore(100);
        participant2.updateScore(80);
        assertEquals(100, participant1.getScore());
        assertEquals(80, participant2.getScore());
    }

    @Test
    public void testIncrementScore() {

        participant1.updateScore(100);
        assertEquals(100, participant1.getScore());
        participant1.incrementScore(50);
        assertEquals(150, participant1.getScore());
    }

    @Test
    public void testUpdateScore() {

        participant1.updateScore(100);
        participant2.updateScore(80);
        assertEquals(100, participant1.getScore());
        assertEquals(80, participant2.getScore());

    }

    @Test
    public void testGetClone() {
        assertTrue(participant1.equals(participant1.getClone()));
    }

    @Test
    public void equalsTest() {

        assertTrue(participant1.equals(participant1));
        assertTrue(participant1.equals(participant1.getClone()));
        assertFalse(participant1.equals("test"));
        assertFalse(participant1.equals(null));

    }

}
