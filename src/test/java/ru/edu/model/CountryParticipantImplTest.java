package ru.edu.model;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.Competition;
import java.util.List;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class CountryParticipantImplTest extends TestCase {

    private CountryParticipantImpl countryParticipant1;
    private CountryParticipantImpl countryParticipant2;
    private AthleteImpl athlete1;
    private AthleteImpl athlete2;
    private AthleteImpl athlete3;
    private Participant participant1;
    private Participant participant2;
    private Participant participant3;

    @Mock
    private Competition testCompetition;


    @Before
    public void setUp() {

        openMocks(this);

        countryParticipant1 = new CountryParticipantImpl("USA");
        countryParticipant2 = new CountryParticipantImpl("Japan");

        athlete1 = new AthleteImpl.Builder()
                .setFirstName("Yuma")
                .setLastName("KAGIYAMA")
                .setCountry("Japan")
                .build();

        athlete2 = new AthleteImpl.Builder()
                .setFirstName("Jason")
                .setLastName("BROWN")
                .setCountry("Japan")
                .build();

        athlete3 = new AthleteImpl.Builder()
                .setFirstName("Jason")
                .setLastName("BROWN")
                .setCountry("USA")
                .build();

        participant1 = new ParticipantImpl(1, athlete1);
        participant2 = new ParticipantImpl(2, athlete2);
        participant3 = new ParticipantImpl(3, athlete3);

        testCompetition = mock(Competition.class);

    }


    @Test
    public void testSetScore() {

        countryParticipant1.setScore(80);
        countryParticipant2.setScore(120);

        assertEquals(80,  countryParticipant1.getScore());
        assertEquals(120, countryParticipant2.getScore());
    }

    @Test
    public void testGetName() {

        assertEquals("USA",   countryParticipant1.getName());
        assertEquals("Japan", countryParticipant2.getName());

    }

    @Test
    public void testGetParticipants() {

        when(testCompetition.register(athlete1)).thenReturn(participant1);
        when(testCompetition.register(athlete2)).thenReturn(participant2);
        when(testCompetition.register(athlete3)).thenReturn(participant3);

        countryParticipant2.addParticipant(participant1);
        countryParticipant2.addParticipant(participant2);

        List<Participant> participantsList = countryParticipant2.getParticipants();

        assertEquals(2, participantsList.size());
    }

    public void testGetScore() {

        countryParticipant1.setScore(80);
        countryParticipant2.setScore(120);

        assertEquals(80,  countryParticipant1.getScore());
        assertEquals(120, countryParticipant2.getScore());

    }

    public void testAddParticipant() {
        countryParticipant2.addParticipant(participant2);
        countryParticipant2.addParticipant(participant1);
    }
}