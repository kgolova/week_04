package ru.edu.model;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class AthleteImplTest extends TestCase {

    private AthleteImpl athlete;
    private AthleteImpl athlete2;

    @Before
    public void setUp() {

       athlete = new AthleteImpl.Builder()
               .setFirstName("Yuma")
               .setLastName("KAGIYAMA")
               .setCountry("Japan")
               .build();

        athlete2 = new AthleteImpl.Builder()
                .setFirstName("Jason")
                .setLastName("BROWN")
                .setCountry("USA")
                .build();

    }

    @Test
    public void testGetFirstName() {
        assertEquals("Yuma",        athlete.getFirstName());
        assertEquals("Jason",       athlete2.getFirstName());
    }

    @Test
    public void testGetLastName() {
        assertEquals("KAGIYAMA",    athlete.getLastName());
        assertEquals("BROWN",       athlete2.getLastName());
    }

    @Test
    public void testGetCountry() {
        assertEquals("Japan",       athlete.getCountry());
        assertEquals("USA",         athlete2.getCountry());
    }

    @Test
    public void testBuilder() {
        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("Nathan")
                .setLastName("CHEN")
                .setCountry("USA")
                .build();
    }

    @Test
    public void testTestEquals() {

        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("Yuma")
                .setLastName("KAGIYAMA")
                .setCountry("Japan")
                .build();

        assertTrue(athlete.equals(athlete3));
    }

    @Test
    public void testTestEqualsThisObj() {

        assertTrue(athlete.equals(athlete));
    }

    @Test
    public void testTestEqualsOtherClass() {

        String athlete3 = "athlete3";

        assertFalse(athlete.equals(athlete3));
    }

    @Test
    public void testTestEqualsFirstName() {

        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("neYuma")
                .setLastName("KAGIYAMA")
                .setCountry("Japan")
                .build();

        assertFalse(athlete.equals(athlete3));
    }

    @Test
    public void testTestEqualsLastName() {

        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("Yuma")
                .setLastName("neKAGIYAMA")
                .setCountry("Japan")
                .build();

        assertFalse(athlete.equals(athlete3));
    }

    @Test
    public void testTestEqualsCountry() {

        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("Yuma")
                .setLastName("KAGIYAMA")
                .setCountry("neJapan")
                .build();

        assertFalse(athlete.equals(athlete3));
    }

    @Test
    public void testTestEqualsNull() {

        assertFalse(athlete.equals(null));
    }

    @Test
    public void testTestHashCode() {
        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("Yuma")
                .setLastName("KAGIYAMA")
                .setCountry("Japan")
                .build();

        assertEquals(athlete.hashCode(), athlete3.hashCode());

    }

    @Test
    public void testTestToString() {
        assertEquals("Имя=Yuma Фамилия=KAGIYAMA (Страна=Japan)",    athlete.toString());
        assertEquals("Имя=Jason Фамилия=BROWN (Страна=USA)",        athlete2.toString());
    }
}

