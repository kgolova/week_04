package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import ru.edu.model.AthleteImpl;
import ru.edu.model.CountryParticipant;
import ru.edu.model.Participant;
import java.util.List;


public class CompetitionImplTest extends TestCase {

    private AthleteImpl athlete1;
    private AthleteImpl athlete2;
    private AthleteImpl athlete3;
    private AthleteImpl athlete4;
    private Participant participant1;
    private Participant participant2;
    private Participant participant3;
    private CompetitionImpl testCompetition;


    @Before
    public void setUp() {


        athlete1 = new AthleteImpl.Builder()
                .setFirstName("Yuma")
                .setLastName("KAGIYAMA")
                .setCountry("Japan")
                .build();

        athlete2 = new AthleteImpl.Builder()
                .setFirstName("Jason")
                .setLastName("BROWN")
                .setCountry("USA")
                .build();

        athlete3 = new AthleteImpl.Builder()
                .setFirstName("Test")
                .setLastName("TEST")
                .setCountry("Japan")
                .build();

        testCompetition = new CompetitionImpl();

        participant1 = testCompetition.register(athlete1);
        participant2 = testCompetition.register(athlete2);
        participant3 = testCompetition.register(athlete3);


    }

    @Test
    public void testRegister() {

         athlete4 = new AthleteImpl.Builder()
                .setFirstName("Test1")
                .setLastName("TEST1")
                .setCountry("Japan")
                .build();

        testCompetition.register(athlete4);

        assertEquals(4, testCompetition.getParticipantMapSize());

    }

    @Test
    public void testRegisterDuplicate() {

        boolean success = false;
        try {
            testCompetition.register(athlete1);
        } catch (IllegalStateException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testUpdateScoreID() {

        assertEquals(0, participant1.getScore());
        assertEquals(0, participant2.getScore());

        testCompetition.updateScore(participant1.getId(), 10);
        testCompetition.updateScore(participant2.getId(), 15);

        participant1 = testCompetition.getParticipant(participant1.getId());
        participant2 = testCompetition.getParticipant(participant2.getId());

        assertEquals(10, participant1.getScore());
        assertEquals(15, participant2.getScore());

    }

    @Test
    public void testUpdateScoreIDNull() {

        boolean success = false;
        try {
            testCompetition.updateScore(99, 10);
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testUpdateScoreParticipant() {

        assertEquals(0, participant1.getScore());
        assertEquals(0, participant2.getScore());

        testCompetition.updateScore(participant1, 10);
        testCompetition.updateScore(participant2, 15);

        participant1 = testCompetition.getParticipant(participant1.getId());
        participant2 = testCompetition.getParticipant(participant2.getId());

        assertEquals(10, participant1.getScore());
        assertEquals(15, participant2.getScore());

    }


    @Test
    public void testGetResults() {

        testCompetition.updateScore(participant1.getId(), 10);
        testCompetition.updateScore(participant2.getId(), 15);

        List<Participant> participantsResult =  testCompetition.getResults();

        assertEquals(15,        participantsResult.get(0).getScore());
        assertEquals("Jason",   participantsResult.get(0).getAthlete().getFirstName());
        assertEquals(10,        participantsResult.get(1).getScore());
        assertEquals("Yuma",    participantsResult.get(1).getAthlete().getFirstName());

    }

    @Test
    public void testGetParticipantsCountries() {


        testCompetition.updateScore(participant1.getId(), 10);
        testCompetition.updateScore(participant2.getId(), 15);
        testCompetition.updateScore(participant3.getId(), 40);

        List <CountryParticipant> participantsCountries = testCompetition.getParticipantsCountries();

        assertEquals(50,        participantsCountries.get(0).getScore());
        assertEquals("Japan",   participantsCountries.get(0).getName());
        assertEquals(2,         participantsCountries.get(0).getParticipants().size());
        assertEquals(15,        participantsCountries.get(1).getScore());
        assertEquals("USA",     participantsCountries.get(1).getName());
        assertEquals(1,         participantsCountries.get(1).getParticipants().size());
    }
}